#include <stdio.h>
#include "png_util.h"
#define CUDA_CALL(x) {cudaError_t cuda_error__ = (x); if (cuda_error__) printf("CUDA error: " #x " returned \"%s\"\n", cudaGetErrorString(cuda_error__));}

const int N = 16; 

__global__ void hello(char *a) 
{
    a[threadIdx.x] = 44;
}

int main()
{
    struct cudaDeviceProp properties;
    cudaGetDeviceProperties(&properties, 0);
    printf("using %d multiprocessors\n",properties.multiProcessorCount);
    printf("max threads per processor: %d \n\n",properties.maxThreadsPerMultiProcessor);

    char * a;
    const int csize = N*sizeof(char);

    a = (char *) malloc(csize); 
    for (int i=0; i < N;i++ )
        a[i] = 7;

    char *ad;
    CUDA_CALL(cudaMalloc( (void**) &ad, csize )); 
    CUDA_CALL(cudaMemcpy( ad, a, csize, cudaMemcpyHostToDevice )); 

    dim3 dimBlock( N, 1 );
    dim3 dimGrid( 1, 1 );

    hello<<<dimGrid, dimBlock>>>(ad);

    CUDA_CALL(cudaMemcpy( a, ad, csize, cudaMemcpyDeviceToHost )); 
    cudaFree( ad );

    for (int i=0; i < N; i++)
        printf("%d ", a[i]);
    printf("\n");
    return EXIT_SUCCESS;
}
