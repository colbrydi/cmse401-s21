#include <stdio.h>
#include "png_util.h"
#define CUDA_CALL(x) {cudaError_t cuda_error__ = (x); if (cuda_error__) printf("CUDA error: " #x " returned \"%s\"\n", cudaGetErrorString(cuda_error__));}

const int N = 16;

__global__ void hello(char *a)
{ 
   a[threadIdx.x] =  5;
}

int main() { 
    struct cudaDeviceProp properties;
    cudaGetDeviceProperties(&properties, 0);
    printf("using %d multiprocessors\n",properties.multiProcessorCount);
    printf("max threads per processor: %d \n\n",properties.maxThreadsPerMultiProcessor);

    char * a;
    const int nBytes = N*sizeof(char);

    a = (char *) malloc(nBytes);
    for(int k=0;k < N;k++)
	a[k] = 0;
       
    //CUDA Memory malloc 
    char *ad;
    CUDA_CALL(cudaMalloc((void **) &ad, nBytes));
    CUDA_CALL(cudaMemcpy(ad, a, nBytes, cudaMemcpyHostToDevice));

    dim3 dimBlock(N,1);
    dim3 dimGrid(1,1);

    hello<<<dimGrid, dimBlock>>>(ad);
    
    CUDA_CALL(cudaMemcpy(a, ad, nBytes, cudaMemcpyDeviceToHost));
    cudaFree(ad);

    return 0;
}
