#!/bin/bash

for job in `ls *.out | cut -d "-" -f2 | cut -d "." -f1`;
do
	echo $job
	squeue -j $job
done

